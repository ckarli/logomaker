import base64

import cStringIO

from django.http import JsonResponse
from django.shortcuts import render
from PIL import Image

def is_transparent(image):
    return image.mode in ('RGBA', 'LA') or (image.mode == 'P' and 'transparency' in image.info)

def index(request):
    context = {}

    if request.POST:
        default_width = int(request.POST.get('width', 200))
        default_height = int(request.POST.get('height', 60))
        transparency = not (request.POST.get('transparency', "") == "")
        defaultsize = default_width, default_height
        color = request.POST.get('color', "#FFFFFF").lstrip('#')
        color_rgb = tuple(int(color[i:i + 2], 16) for i in (0, 2, 4))

        profile_pic = request.FILES.get('cropped_image',"")
        poster_wip = Image.open(profile_pic)
        img_info = poster_wip.info

        hpercent = (default_height / float(poster_wip.size[1]))

        wsize = int((float(poster_wip.size[0]) * float(hpercent)))
        x, y = poster_wip.size

        poster_wip = poster_wip.resize((wsize, default_height), Image.BICUBIC )

        if transparency:

            poster_wip = poster_wip.convert('RGBA')
            canvas = Image.new('RGBA', defaultsize, (255, 255, 255, 255))
            canvas.paste(poster_wip, (0, 0), mask=poster_wip)
        else:
            if is_transparent(poster_wip):
                if poster_wip.mode != 'RGBA':
                    poster_wip = poster_wip.convert('RGBA')
                canvas = Image.new('RGBA', defaultsize, color_rgb+(255,))

                canvas.paste(poster_wip ,(0, 0), mask=poster_wip)
            else:
                canvas = Image.new(poster_wip.mode, defaultsize, color_rgb)

                canvas.paste(poster_wip,(0,0))



        resized_poster_file = cStringIO.StringIO()

        if transparency:

            canvas.save(resized_poster_file, "GIF", transparency=0)
        else:
            canvas.save(resized_poster_file, "GIF")
        img_str = base64.b64encode(resized_poster_file.getvalue())
        context = {'image': "%s%s"%("data:image/gif;base64,",img_str)}
        if request.is_ajax():
            return JsonResponse(context)


    return render(request, 'resizer/index.html', context)
